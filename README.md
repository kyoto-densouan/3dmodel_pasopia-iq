# README #

1/3スケールの東芝 PASOPIA IQ・HX-10D風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK Fusion360です。


***

# 実機情報

## メーカ
- 東芝

## 発売時期
- 1983年

## 類似モデル
- HX-10
- HX-10P
- HX-10DP

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%91%E3%82%BD%E3%83%94%E3%82%A2)
- [懐かしのホビーパソコン紹介](https://mobile.twitter.com/i/events/794732853447659520)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_pasopia-iq/raw/bb6fa37a80fd208ed071a7bbdd2136fcec63eca2/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pasopia-iq/raw/bb6fa37a80fd208ed071a7bbdd2136fcec63eca2/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pasopia-iq/raw/bb6fa37a80fd208ed071a7bbdd2136fcec63eca2/ExampleImage.jpg)
